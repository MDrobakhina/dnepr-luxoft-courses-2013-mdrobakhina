package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 14.12.13
 * Time: 19:16
 * To change this template use File | Settings | File Templates.
 */
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            Cookie[] cookies = req.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("login")) {
                    req.setAttribute("login", cookie.getValue());
                }
            }

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("LoginSuccess.jsp");
            requestDispatcher.forward(req, resp);
    }
}
