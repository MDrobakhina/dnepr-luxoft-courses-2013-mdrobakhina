package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class TestServlet extends HttpServlet {
    private int hitCount = 0;

    protected void doGet(HttpServletRequest request
            , HttpServletResponse response)
            throws ServletException, IOException {


        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("{\"hitCount\": ");
        stringBuilder.append(++hitCount);
        stringBuilder.append("}");

        PrintWriter writer = response.getWriter();
        writer.write(stringBuilder.toString());

        response.setContentLength(stringBuilder.length());
    }
}
