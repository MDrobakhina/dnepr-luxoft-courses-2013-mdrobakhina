package com.luxoft.dnepr.courses.luxoftWeb;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 14.12.13
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */

@WebServlet
        (
                description = "Login Servlet",
                urlPatterns = {"/LoginServlet"}
        )

public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //get request parameters for userID and password
        String user = request.getParameter("user");
        String pwd = request.getParameter("pwd");

        //logging example
        log("User=" + user + "::password=" + pwd);

        if (isUserExists(user, pwd)) {
            Cookie cookie = new Cookie("login", user);
            cookie.setMaxAge(180);
            response.addCookie(cookie);
            response.sendRedirect("user");
        } else {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.html");
            PrintWriter out = response.getWriter();
            out.println("<font color=red>Either user name or password is wrong.</font>");
            rd.include(request, response);
        }
    }

    private boolean isUserExists(String user, String pass) {
        String usersFileName = getServletContext().getRealPath("/") + "META-INF/" + getServletContext().getInitParameter("users");

        try {
            File fXmlFile = new File(usersFileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("user");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    String attrName = eElement.getAttribute("name");
                    String attrPass = eElement.getAttribute("password");

                    if (user.equals(attrName)) {
                        if (pass.equals(attrPass)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return false;
    }

}