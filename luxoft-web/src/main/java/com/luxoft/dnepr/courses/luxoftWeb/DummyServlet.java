package com.luxoft.dnepr.courses.luxoftWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 07.12.13
 * Time: 23:12
 * To change this template use File | Settings | File Templates.
 */
public class DummyServlet extends HttpServlet {

    private HashMap <String, String> persons = new HashMap<String, String>();

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        if (name == null || age == null) {
            resp.setStatus(500);
            resp.setContentType("application/json");
            resp.setCharacterEncoding("utf-8");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{\"error\": \"Illegal parameters\"}");
            resp.setContentLength(stringBuilder.length());
            resp.getWriter().write(stringBuilder.toString());
        } else if (persons.containsKey(name)) {
            resp.setStatus(500);
            resp.setContentType("application/json");
            resp.setCharacterEncoding("utf-8");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{\"error\": \"Name ");
            stringBuilder.append(name);
            stringBuilder.append(" already exists\"}");
            resp.setContentLength(stringBuilder.length());
            resp.getWriter().write(stringBuilder.toString());
        } else {
            resp.setStatus(201);
            persons.put(name, age);
        }
    }

    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //String name = req.getParameter("name");
        //String age = req.getParameter("age");
        InputStreamReader inputStreamReader = new InputStreamReader(req.getInputStream());
        BufferedReader reader = new BufferedReader(inputStreamReader);
        String params = reader.readLine();



        if (params.indexOf('&') == -1) {
            resp.setStatus(500);
            resp.setContentType("application/json");
            resp.setCharacterEncoding("utf-8");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{\"error\": \"Illegal parameters\"}");
            resp.setContentLength(stringBuilder.length());
            resp.getWriter().write(stringBuilder.toString());
        } else {
            String param1 = params.substring(0, params.indexOf('&'));
            String param2 = params.substring(params.indexOf('&') + 1, params.length());

            String param1_key = param1.substring(0, param1.indexOf('='));
            String param1_value = param1.substring(param1.indexOf('=') + 1, param1.length());

            String param2_key = param2.substring(0, param2.indexOf('='));
            String param2_value = param2.substring(param2.indexOf('=') + 1, param2.length());

           if ((!param1_key.equals("name") && !param2_key.equals("name")) ||
                (!param1_key.equals("age") && !param2_key.equals("age"))) {
                resp.setStatus(500);
                resp.setContentType("application/json");
                resp.setCharacterEncoding("utf-8");
                StringBuilder stringBuilder = new StringBuilder();
               stringBuilder.append("{\"error\": \"Illegal parameters\"}");
                resp.setContentLength(stringBuilder.length());
                resp.getWriter().write(stringBuilder.toString());
            } else {
                String name;
                String age;

                if (param1_key.equals("name")) {
                    name = param1_value;
                    age = param2_value;
                } else {
                    name = param2_value;
                    age = param1_value;
                }

                if (!persons.containsKey(name)) {
                    resp.setStatus(500);
                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("utf-8");
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("{\"error\": \"Name ");
                    stringBuilder.append(name);
                    stringBuilder.append(" does not exist\"}");
                    resp.setContentLength(stringBuilder.length());
                    resp.getWriter().write(stringBuilder.toString());
                } else {
                    resp.setStatus(202);
                    persons.put(name, age);
                }
            }
        }
    }
}
