package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameController {
    private static GameController controller;
    private static final int MAX_SCORE = 21;
    private static final int DEALER_SCORE = 17;
    private List<Card> deck;
    private ArrayList<Card> handPlayer;
    private ArrayList<Card> handDealer;


    private GameController() {
        // implemented
        handDealer = new ArrayList<Card>();
        handPlayer = new ArrayList<Card>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }
        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {
        // implemented.
        deck = Deck.createDeck(1);
        shuffler.shuffle(deck);
        handDealer.clear();
        handPlayer.clear();
        handPlayer.add(deck.remove(0));
        handPlayer.add(deck.remove(0));
        handDealer.add(deck.remove(0));

    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        // implemented
        if (Deck.costOf(handPlayer) <= MAX_SCORE && deck.size() > 0) {
            handPlayer.add(deck.remove(0));
        }
        return Deck.costOf(handPlayer) <= MAX_SCORE;
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        // implemented
        while (deck.size() > 0 && Deck.costOf(handDealer) < DEALER_SCORE) {
            handDealer.add(deck.remove(0));
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        // implemented
        int playerCost = Deck.costOf(handPlayer);
        int dealerCost = Deck.costOf(handDealer);
        WinState state = WinState.WIN;   //returns if the dealer has too much
        if (playerCost > MAX_SCORE) {
            state = WinState.LOOSE;
        } else if (playerCost > dealerCost) {
            state = WinState.WIN;
        } else if (playerCost == dealerCost) {
            state = WinState.PUSH;
        } else if (dealerCost < MAX_SCORE) {
            state = WinState.LOOSE;
        }
        return state;
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        // implemented
        return handPlayer;
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        // implemented
        return handDealer;
    }
}
