package com.luxoft.dnepr.courses.unit3.model;

public enum WinState {
	WIN("Congrats! You win!"),
	LOOSE("Sorry, today is not your day. You loose."),
	PUSH("Push. Everybody has equal amount of points.");

    private String description;

    public String getDescription() {
        return description;
    }
    private WinState(String description) {
        this.description=description;

    }

}
