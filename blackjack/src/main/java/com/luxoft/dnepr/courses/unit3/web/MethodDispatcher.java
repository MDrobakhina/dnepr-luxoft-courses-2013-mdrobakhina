package com.luxoft.dnepr.courses.unit3.web;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

import java.util.List;

public class MethodDispatcher {

	/**
	 * 
	 * @param request
	 * @param response
	 * @return response or <code>null</code> if wasn't able to find a method.
	 */
	public Response dispatch(Request request, Response response) {
		String method = request.getParameters().get("method");
		if (method == null) {
			return null;
		}
		if (method.equals("requestMore")) {
			return requestMore(response);
		}

		return null;
	}

	private Response requestMore(Response response) {
		response.write("{ result: " + GameController.getInstance().requestMore());
		response.write(", myhand: ");

		writeHand(response, GameController.getInstance().getMyHand());

		response.write("}");

		return response;
	}

	private void writeHand(Response response, List<Card> hand) {
		boolean isFirst = true;
		response.write("[");
		for (Card card : hand) {
			if (isFirst) {
				isFirst = false;
			} else {
				response.write(",");
			}
			response.write("{rank: '");
			response.write(card.getRank().getName());
			response.write("', suit: '");
			response.write(card.getSuit().name());
			response.write("', cost: ");
			response.write(String.valueOf(card.getCost()));
			response.write("}");
		}
		response.write("]");
	}

}
