package com.luxoft.dnepr.courses.unit3.controller;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

import java.util.ArrayList;
import java.util.List;

public final class Deck {
    private Deck() {
    }

    public static List<Card> createDeck(int size) {
        // implemented
        ArrayList<Card> deck = new ArrayList<Card>();
        if (size < 1) {
            size = 1;
        } else if (size > 10) {
            size = 10;
        }
        for (int deckN = 0; deckN < size; deckN++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    deck.add(new Card(rank, suit));
                }
            }
        }

        return deck;
    }

    public static int costOf(List<Card> hand) {
        // implemented
        int cost = 0;
        for (int i = 0; i < hand.size(); i++)
            cost += hand.get(i).getCost();
        return cost;
    }
}
