package com.luxoft.dnepr.courses.unit3.view;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

public class CommandLineInterface {

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: (MDrobakhina)\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();

        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * @see com.luxoft.dnepr.courses.unit3.view.CommandLineInterfaceTest
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *      после того как диллер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры
     *      <p/>
     *      Состояние:
     *      рука игрока (total вес)
     *      рука диллера (total вес)
     *      <p/>
     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)
     *      <p/>
     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      <p/>
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */
    private boolean execute(String command, GameController controller) {
        // implemented
        boolean result = true;
        if (command.equalsIgnoreCase(Command.MORE)) {
            result = requestMore(controller);
        } else if (command.equalsIgnoreCase(Command.STOP)) {
            requestStop(controller);
            result = false;
        } else if (command.equalsIgnoreCase(Command.EXIT)) {
            result = false;
        } else if (command.equalsIgnoreCase(Command.HELP)) {
            output.print("Usage: \n" +
                    "\thelp - prints this message\n" +
                    "\thit - requests one more card\n" +
                    "\tstand - I'm done - lets finish\n" +
                    "\texit - exits game\n");
        } else {
            output.println("Invalid command");
        }


        return result;
    }


    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();

        format(myHand);

        List<Card> dealersHand = controller.getDealersHand();

        format(dealersHand);
    }

    private void format(List<Card> hand) {
        int totalCost = 0;
        for (Card card : hand) {
            output.format("%s ", card.getRank().getName());
            totalCost += card.getCost();
        }
        output.format("(total %d)\n", totalCost);

    }

    private boolean requestMore(GameController controller) {
        Boolean success = controller.requestMore();
        printState(controller);
        if (!success) {
            output.println();
            output.print(WinState.LOOSE.getDescription());
        }
        return success;

    }

    private void requestStop(GameController controller) {
        controller.requestStop();
        output.print("Dealer turn:\n");
        output.print("\n");
        printState(controller);
        output.print("\n");
        output.print(controller.getWinState().getDescription());
    }
}
