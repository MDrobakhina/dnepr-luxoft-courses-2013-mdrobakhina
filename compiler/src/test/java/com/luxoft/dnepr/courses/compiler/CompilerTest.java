package com.luxoft.dnepr.courses.compiler;

import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(4.5, "  9 /2 ");
        assertCompiled(5.1, "  10.2 /2 ");
        assertCompiled(500.00, "  1000.00/2");

    }

    @Test
    public void testExpectedException() {
        try {
            Compiler.compile("2*+2");
            assert false;
        } catch (CompilationException e) {
            assert true;
        }
        try {
            Compiler.compile(null);
            assert false;
        } catch (CompilationException e) {
            assert true;
        }
        try {
            Compiler.compile("bhjbjbjbjjhbjj");
            assert false;
        } catch (CompilationException e) {
            assert true;
        }
        try {
            Compiler.compile("2+2a");
            assert false;
        } catch (CompilationException e) {
            assert true;
        }

    }


}
