package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Compiler {
    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        int i = 0;
        double num1;
        double num2;
        char operSign;
        if (input == null) {
            throw new CompilationException("The input is null");
        }
        input.replace(" ", "");
        String firstNum = getNumber(input, i);
        try {
            num1 = Double.parseDouble(firstNum);
        } catch (NumberFormatException e) {
            throw new CompilationException("The input has illegal symbols");
        }
        addCommand(result, VirtualMachine.PUSH, num1);
        i += firstNum.length();
        try {
            operSign = input.charAt(i);
        } catch (StringIndexOutOfBoundsException ex) {
            throw new CompilationException("Missing the sign");
        }
        i++;
        String secondNum = getNumber(input, i);
        try {
            num2 = Double.parseDouble(secondNum);
        } catch (NumberFormatException e) {
            throw new CompilationException("The input has illegal symbols");
        }
        addCommand(result, VirtualMachine.PUSH, num2);
        addOperationCommands(operSign, result);
        addCommand(result, VirtualMachine.PRINT);
        return result.toByteArray();
    }

    public static boolean isOperation(char input) {
        char[] operation = new char[]{'+', '-', '*', '/'};
        boolean result = false;
        for (char cur : operation) {
            if (input == cur) {
                result = true;
                break;
            }
        }
        return result;
    }

    public static String getNumber(String input, int i) {
        String tempNumber = "";
        while (i < input.length() && !isOperation(input.charAt(i))) {
            tempNumber += input.charAt(i);
            i++;
        }
        return tempNumber;
    }

    public static void addOperationCommands(char operSign, ByteArrayOutputStream result) {
        switch (operSign) {
            case '+':
                addCommand(result, VirtualMachine.ADD);
                break;
            case '-':
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.SUB);
                break;
            case '*':
                addCommand(result, VirtualMachine.MUL);
                break;
            case '/':
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.DIV);
                break;
        }
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
