package com.luxoft.dnepr.courses.unit2.model;

abstract public class Figure {
    abstract public double calculateArea();
}

