package com.luxoft.dnepr.courses.unit2.model;


public class Circle extends Figure {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double calculateArea() {
         return Math.PI*Math.pow(radius,2);
    }
}
