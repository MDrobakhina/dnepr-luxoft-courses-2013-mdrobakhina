package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 13.11.13
 * Time: 21:13
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstactDao<E extends Entity> implements IDao<E>{
    public AbstactDao(){

    }
    public E save (E e)  {
        if (e != null) {
            if (e.getId() == null) {
                e.setId(new Long(EntityStorage.maxId()+1));
            }

            if (EntityStorage.getEntities().containsKey(e.getId())) {
                throw new UserAlreadyExist("User already exists");
            }

            EntityStorage.getEntities().put(e.getId(), e);
        }
        return e;
    }

    public E update (E e) {
        Long id = e.getId();
        if (id == null || !EntityStorage.getEntities().containsKey(id)) {
            throw new UserNotFound("User not found");
        }

        EntityStorage.getEntities().put(e.getId(), e);

        return e;
    }

    public E get (long id) {
        if (EntityStorage.getEntities().containsKey(new Long(id))) {
            return (E)EntityStorage.getEntities().get(new Long(id));
        } else {
            return null;
        }
    }

    public boolean delete (long id) {
        boolean result = false;
        if (EntityStorage.getEntities().containsKey(new Long(id))) {
            EntityStorage.getEntities().remove(new Long(id));
            result = true;
        }
        return result;
    }


}
