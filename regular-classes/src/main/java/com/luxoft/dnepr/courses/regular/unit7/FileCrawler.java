package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.*;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    private WordStorage wordStorage = new WordStorage();
    private int maxNumberOfThreads;
    private String rootFolder;

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    private class TXTFileFilter implements FileFilter {
        @Override
        public boolean accept(File file) {
            if (file.isDirectory()) {
                return true;
            } else {
                String path = file.getAbsolutePath().toLowerCase();
                String extension = ".txt";
                if ((path.endsWith(extension))) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        WordStorage wordStorage = new WordStorage();
        BlockingQueue<String> filesQueue = new LinkedBlockingQueue<String>();
        FileFilter fileFilter = new TXTFileFilter();

        ExecutorService es = Executors.newFixedThreadPool(maxNumberOfThreads);
        Future<HashSet<File>> crawlerCall = es.submit(new FileProducer(filesQueue, fileFilter, new File(rootFolder)));

        for (int i = 0; i < maxNumberOfThreads - 1; i++) {
            es.submit(new FileConsumer(filesQueue, wordStorage));
        }

        es.shutdown();

        try {
            while (!es.awaitTermination(24L, TimeUnit.HOURS)) {
                System.out.println("Still waiting for the executor to finish");
            }
        } catch (Exception exc) {

        }

        HashSet<File> processedFiles = null;

        try {
            processedFiles = crawlerCall.get();
        } catch (Exception exc) {

        }


        return new FileCrawlerResults(new ArrayList<File>(processedFiles), wordStorage.getWordStatistics());
    }


    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }

}
