package com.luxoft.dnepr.courses.unit1;


 final class LuxoftUtils {
    public static String language = "en";

    private LuxoftUtils() {
    }

    public static String getMonthName(int monthNumber, String lang) {

        switch (monthNumber) {
            case 1: {
                if (lang.equals("en"))
                    return "January";
                else if (lang.equals("ru"))
                    return "Январь";
                else return "Unknown language";

            }
            case 2: {
                if (lang.equals("en"))
                    return "February";
                else if (lang.equals("ru"))
                    return "Февраль";
                else return "Unknown language";

            }
            case 3: {
                if (lang.equals("en"))
                    return "March";
                else if (lang.equals("ru"))
                    return "Март";
                else return "Unknown language";

            }
            case 4: {
                if (lang.equals("en"))
                    return "April";
                else if (lang.equals("ru"))
                    return "Апрель";
                else return "Unknown language";

            }
            case 5: {
                if (lang.equals("en"))
                    return "May";
                else if (lang.equals("ru"))
                    return "Май";
                else return "Unknown language";

            }
            case 6: {
                if (lang.equals("en"))
                    return "June";
                else if (lang.equals("ru"))
                    return "Июнь";
                else return "Unknown language";

            }
            case 7: {
                if (lang.equals("en"))
                    return "July";
                else if (lang.equals("ru"))
                    return "Июль";
                else return "Unknown language";

            }
            case 8: {
                if (lang.equals("en"))
                    return "August";
                else if (lang.equals("ru"))
                    return "Август";
                else return "Unknown language";

            }
            case 9: {
                if (lang.equals("en"))
                    return "September";
                else if (lang.equals("ru"))
                    return "Сентябрь";
                else return "Unknown language";

            }
            case 10: {
                if (lang.equals("en"))
                    return "October";
                else if (lang.equals("ru"))
                    return "Октябрь";
                else return "Unknown language";

            }
            case 11: {
                if (lang.equals("en"))
                    return "November";
                else if (lang.equals("ru"))
                    return "Ноябрь";
                else return "Unknown language";

            }
            case 12: {
                if (lang.equals("en"))
                    return "December";
                else if (lang.equals("ru"))
                    return "Декабрь";
                else return "Unknown language";

            }
            default: {
                if (lang.equals("en")) return "Unknown month";
                else if (lang.equals("ru"))
                    return "Неизвестный месяц";
                else return "Unknown language";
            }
        }


    }

    public static String binaryToDecimal(String strBinaryNum) {
        if (strBinaryNum != "") {
            int resultDecimal = 0;
            int power = 0;
            for (int i = strBinaryNum.length() - 1; i >= 0; i--) {
                if (strBinaryNum.charAt(i) != '0' && strBinaryNum.charAt(i) != '1')
                    return "Not binary";
                else if (strBinaryNum.charAt(i) == '0') power++;
                else resultDecimal += Math.pow(2, power++);


            }
            return Integer.toString(resultDecimal);
        } else return "Not binary";

    }

    public static String decimalToBinary(String strDecimalNum) {

        int decimalNum;
        try {
            decimalNum = Integer.decode(strDecimalNum);
        } catch (NumberFormatException e) {
            return "Not decimal";
        }
        decimalNum = Math.abs(decimalNum);
        String binary = "";
        do {
            binary += decimalNum % 2;
            decimalNum /= 2;

        } while (decimalNum > 0);
        String resultBinary = "";
        for (int i = 0; i < binary.length(); i++)
            resultBinary += binary.charAt(binary.length() - 1 - i);
        return resultBinary;


    }

    public static int[] sortArray(int[] array, boolean order) {
        int[] arrayNum = array.clone();
        for (int i = 0; i < arrayNum.length; i++)
            for (int j = 1; j < arrayNum.length - i; j++) {
                if (order) {
                    if (arrayNum[j] < arrayNum[j - 1]) {
                        arrayNum[j - 1] += arrayNum[j];
                        arrayNum[j] = arrayNum[j - 1] - arrayNum[j];
                        arrayNum[j - 1] -= arrayNum[j];
                    }
                } else if (arrayNum[j] > arrayNum[j - 1]) {
                    arrayNum[j - 1] += arrayNum[j];
                    arrayNum[j] = arrayNum[j - 1] - arrayNum[j];
                    arrayNum[j - 1] -= arrayNum[j];
                }
            }

        return arrayNum;
    }





}
