package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.List;

public final class LuxoftUtils {

    private LuxoftUtils() {
    }

    public static String[] sortArray(String[] array, boolean asc) {
        String[] arrayTemp = array.clone();
        LuxoftUtils.mergeSort(arrayTemp, 0, arrayTemp.length - 1, asc);
        return arrayTemp;
    }

    public static void mergeSort(String[] arrayTemp, int startIndex, int lastIndex, boolean asc) {
        int splitIndex = 0;
        if (startIndex < lastIndex) {
            splitIndex = (startIndex + lastIndex) / 2;
            mergeSort(arrayTemp, startIndex, splitIndex, asc);
            mergeSort(arrayTemp, splitIndex + 1, lastIndex, asc);
            mergeSortArrays(arrayTemp, startIndex, splitIndex, lastIndex, asc);
        }
    }

    public static void mergeSortArrays(String[] arrayTemp, int startIndex, int splitIndex, int lastIndex, boolean asc) {

        int pos1 = startIndex;
        int pos2 = splitIndex + 1;
        int posTempBuffer = 0;
        String[] tempBuffer = new String[arrayTemp.length];
        int flagAsc = 1;
        if (!asc) {
            flagAsc = -1;
        }
        while (pos1 <= splitIndex && pos2 <= lastIndex) {
            if (arrayTemp[pos1].compareTo(arrayTemp[pos2]) * flagAsc < 0) {
                tempBuffer[posTempBuffer++] = arrayTemp[pos1++];
            } else {
                tempBuffer[posTempBuffer++] = arrayTemp[pos2++];
            }
        }

        while (pos2 <= lastIndex)
            tempBuffer[posTempBuffer++] = arrayTemp[pos2++];
        while (pos1 <= splitIndex)
            tempBuffer[posTempBuffer++] = arrayTemp[pos1++];
        for (int i = 0; i < posTempBuffer; i++)
            arrayTemp[startIndex + i] = tempBuffer[i];

    }

    public static double wordAverageLength(String str) {
        if (!str.equals("")) {
            int letterNum = 0;
            int wordsNum = 0;
            int i = 0;
            int len = str.length();
            while (str.charAt(i) == ' ') {
                if (i < len - 1) {
                    i++;
                } else {
                    return 0;
                }
            }
            while (i < len) {
                if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z' || str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
                    letterNum++;
                    if (i == len - 1) {
                        wordsNum++;
                        break;
                    }
                    i++;

                } else {
                    wordsNum++;
                    while (i < len && str.charAt(i) == ' ') {
                        i++;
                    }
                }

            }
            return 1.0 * letterNum / wordsNum;
        } else {
            return 0;
        }
    }

    public static double calculateOverallArea(List<Figure> figures) {
        double area = 0;

        for (Figure figure : figures) {
            area += figure.calculateArea();
        }

        return area;
    }

    public static String reverseWords(String str) {
        int j = 0;
        char temp;
        int i = 0;
        char[] s = str.toCharArray();
        while (i < s.length) {
            if (str.charAt(i) == ' ') {
                i++;
            } else {
                j = i;
                while (j < s.length && s[j] != ' ') {
                    j++;
                }
                for (int k = i; k < i + (j - i) / 2; k++) {
                    temp = s[k];
                    s[k] = s[j - 1];
                    s[j - 1] = temp;
                }
                i = j + 1;

            }
        }
        String result = new String(s);
        return result;
    }


}

