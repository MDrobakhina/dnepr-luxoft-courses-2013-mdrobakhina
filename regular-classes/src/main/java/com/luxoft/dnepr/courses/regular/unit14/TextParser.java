package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 17.12.13
 * Time: 23:22
 * To change this template use File | Settings | File Templates.
 */
public class TextParser {
    private String fileName;
    private TextParserDelegate delegate;

    public TextParserDelegate getDelegate() {
        return delegate;
    }

    public void setDelegate(TextParserDelegate delegate) {
        this.delegate = delegate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public TextParser() {

    }

    public TextParser(String fileName, TextParserDelegate delegate) {
        this.delegate = delegate;
        this.fileName = fileName;
    }

    public void parse () {
        Scanner scanner = new Scanner(getClass().getResourceAsStream(fileName));
        scanner.useDelimiter("[ ,.;!?:\\n\\r]");
            while (scanner.hasNext()) {
                String nextWord = scanner.next();
                if (nextWord.length() > 3)
                    delegate.parserDidFindWord(nextWord);
            }
    }
}
