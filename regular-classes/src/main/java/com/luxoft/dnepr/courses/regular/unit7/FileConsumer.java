package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FileInputStream;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 23.11.13
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */
public class FileConsumer implements Runnable {
    private final BlockingQueue<String> queue;
    private final WordStorage wordStorage;

    public FileConsumer(BlockingQueue<String> queue, WordStorage wordStorage) {
        this.queue = queue;
        this.wordStorage = wordStorage;
    }

    public void run() {
        try {
            while (true) {
                try {
                    String filePath = queue.take();
                    if (!filePath.equals("q")) {
                        indexFile(filePath);
                    } else {
                        queue.add("q");
                        break;
                    }

                } catch (Exception exc) {
                    throw new InterruptedException();
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void indexFile(String filePath) throws Exception{
        File file = new File(filePath);
        Scanner sc = new Scanner(new FileInputStream(file), "UTF-8");
        sc.useDelimiter("(?U)[^A-Za-z0-9А-Яа-яёЁ]");
        while (sc.hasNext()) {
            String s = sc.next();
            if (!s.equals(""))
                wordStorage.save(s);
        }
    }
}
