package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 11.11.13
 * Time: 21:47
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {
    private final static Map<Long, Entity> entities = new HashMap();
    private EntityStorage() {}
    public static Map<Long, Entity> getEntities() {
        return entities;
    }

    public static Long maxId () {
        Long[] keys = entities.keySet().toArray(new Long[0]);
        long maxId = 0;
        if (keys.length > 0) {
            maxId = keys[0].longValue();
          for (Long l:keys) {
                if (l.longValue() > maxId) {
                    maxId = l.longValue();
                }
            }
        }
        return new Long(maxId);
    }
}
