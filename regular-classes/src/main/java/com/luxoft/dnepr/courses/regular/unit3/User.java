package com.luxoft.dnepr.courses.regular.unit3;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 06.11.13
 * Time: 22:14
 * To change this template use File | Settings | File Templates.
 */
public class User implements UserInterface {
    private Long id;
    private String name;
    private WalletInterface wallet;

    public User (long id, String name, WalletInterface wallet) {
        this.id = new Long(id);
        this.name = name;
        this.wallet = wallet;
    }

    public Long getId() {
           return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public WalletInterface getWallet() {
        return wallet;
    }

    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }
}
