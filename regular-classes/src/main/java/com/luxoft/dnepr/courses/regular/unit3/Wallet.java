package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 06.11.13
 * Time: 21:49
 * To change this template use File | Settings | File Templates.
 */
public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private BigDecimal maxAmount;
    private WalletStatus status;

    public Wallet (long id, long amount, long maxAmount, WalletStatus status)
    {
        this.id = new Long(id);
        this.amount = new BigDecimal(amount);
        this.maxAmount = new BigDecimal(maxAmount);
        this.status = status;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletStatus getStatus(){
        return status;
    }
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (isBlocked()) {
            throw new WalletIsBlockedException(id, "This wallet is blocked");
        }

        if (amountToWithdraw.compareTo(amount)>0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "Your balance is exceeded");
        }
    }

    public void withdraw(BigDecimal amountToWithdraw) {
          amount.subtract(amountToWithdraw);
    }

    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (isBlocked()) {
            throw new WalletIsBlockedException(id, "This wallet is blocked");
        }

        if (amountToTransfer.add(amount).compareTo(maxAmount)>0) {
            throw new LimitExceededException(id, amountToTransfer, amount, "Your limit is exceeded");
        }
    }

    public void transfer(BigDecimal amountToTransfer) {
          amount.add(amountToTransfer);
    }

    public boolean isBlocked (){
        return (this.status != WalletStatus.ACTIVE);
    }
}
