package com.luxoft.dnepr.courses.unit2.model;

public class Square extends Figure {
    private double squareSide;

    public Square(double side) {
        squareSide = side;

    }

    public double calculateArea() {
        return Math.pow(squareSide, 2);
    }

}
