package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

public class PersonImpl implements Person {

    private String name;
    private String ethnicity;
    private Person father;
    private Person mother;
    private Gender gender;
    private int age;

    public PersonImpl(String name, String ethnicity, Person father, Person mother, Gender gender, int age) {
        this.name = name;
        this.ethnicity = ethnicity;
        this.father = father;
        this.mother = mother;
        this.gender = gender;
        this.age = age;
    }


    public PersonImpl() {

    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    @Override
    public void setFather(Person father) {
        this.father = father;
    }

    @Override
    public void setMother(Person mother) {
        this.mother = mother;
    }

    @Override
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getEthnicity() {
		return this.ethnicity;
	}
	
	@Override
	public Person getFather() {
		return this.father;
	}

	@Override
	public Person getMother() {
		return this.mother;
	}

	@Override
	public Gender getGender() {
		return this.gender;
	}

	@Override
	public int getAge() {
		return this.age;
	}

    @Override
    public String toString() {
        return this.name + " " + this.age + " " + this.getGender().getName() + " " +
                this.getEthnicity() + " " + ((this.getFather()==null)?"":this.getFather().toString()) + " " + ((this.getMother()==null)?"":this.getMother().toString()) + " ";
    }
}
