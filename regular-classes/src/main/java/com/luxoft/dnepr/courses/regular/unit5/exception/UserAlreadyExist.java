package com.luxoft.dnepr.courses.regular.unit5.exception;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 11.11.13
 * Time: 22:11
 * To change this template use File | Settings | File Templates.
 */
public class UserAlreadyExist extends RuntimeException {
    public UserAlreadyExist(String message) {
        super(message);
    }
}
