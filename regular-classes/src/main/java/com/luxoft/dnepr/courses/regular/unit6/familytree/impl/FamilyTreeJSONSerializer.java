package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.JSONSerializer;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 16.11.13
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */
public class FamilyTreeJSONSerializer implements JSONSerializer <FamilyTree> {
    @Override
    public FamilyTree objectFromScanner(Scanner scanner) throws Exception{
        scanner.useDelimiter("[{},]");
        scanner.next();


        PersonJSONSerializer jsonSerializer = new PersonJSONSerializer();
        Person root = jsonSerializer.objectFromScanner(scanner);
        scanner.close();

        return FamilyTreeImpl.create(root);
    }

    @Override
    public void saveObjectToStream(OutputStreamWriter streamWriter, FamilyTree object) throws Exception{
        streamWriter.write("{\"root\":{");
        if (object.getRoot() != null) {
            JSONSerializer <Person> jsonSerializer = new PersonJSONSerializer();
            jsonSerializer.saveObjectToStream(streamWriter, object.getRoot());
        }

        streamWriter.write("}");
        streamWriter.write("}");

        streamWriter.flush();
    }
}
