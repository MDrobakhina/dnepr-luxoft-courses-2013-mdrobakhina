package com.luxoft.dnepr.courses.regular.unit14;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 17.12.13
 * Time: 23:45
 * To change this template use File | Settings | File Templates.
 */
public interface TextParserDelegate {
    void parserDidFindWord(String word);
}
