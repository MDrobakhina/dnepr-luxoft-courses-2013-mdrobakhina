package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private List<Product> products = new ArrayList<Product>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     * @param product new product
     */
    public void append(Product product) {
        CompositeProduct productGroup = (CompositeProduct)findGroupForProduct(product);
        if (productGroup == null) {
            productGroup = new CompositeProduct();
            products.add(productGroup);
        }
        productGroup.add(product);
   }

    private Product findGroupForProduct(Product product){
        Product result = null;
        for (Product product1 : products) {
            CompositeProduct compositeProduct = (CompositeProduct)product1;

            if (compositeProduct.firstChild().equals(product)){
                result = compositeProduct;
                break;
            }
        }
        return result;
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     * @return
     */
    public double summarize() {
        double price = 0;

        for (Product product : products) {
            price += product.getPrice();
        }

        return price;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     * @return
     */
    public List<Product> getProducts() {
        Collections.sort(products, ProductsComparator.PRICE_DESCENDING);
        return products;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize() ;
    }

    private static class ProductsComparator {
        public static Comparator<Product> PRICE_DESCENDING = new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                int result = 0;
                if (o1.getPrice() > o2.getPrice())
                    result = -1;
                else if (o1.getPrice() < o2.getPrice())
                    result = 1;

                return result;
            }
        };
    }

}
