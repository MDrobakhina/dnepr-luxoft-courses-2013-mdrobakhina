package com.luxoft.dnepr.courses.regular.unit2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.GregorianCalendar;

/**
 * Main entry point to the program 'Shop'.
 */
public class Shop {
    public static void main(String[] args) throws CloneNotSupportedException {
        ProductFactory productFactory = new ProductFactory();

        Bill bill = new Bill();

        //buy 2 pieces of bread
        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);

        bill.append(bread);
        Bread anotherBread = (Bread) bread.clone();
        anotherBread.setPrice(15);
        bill.append(anotherBread);

        //3 bottles of Cola
        Beverage cola = productFactory.createBeverage("beverage1", "Coca-cola", 5, true);
        bill.append(cola);
        bill.append((Product) cola.clone());
        bill.append((Product) cola.clone());

        //and 1 book about Java
        Book javaBook = productFactory.createBook("book1", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        bill.append(javaBook);

        System.out.println(bill);
    }

    private boolean isUserExists(String user, String pass) {


        try {
            File fXmlFile = new File("users.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("user");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    String attrName = eElement.getAttribute("name");
                    String attrPass = eElement.getAttribute("password");

                    if (user.equals(attrName)) {
                        if (pass.equals(attrPass)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return false;
    }
}
