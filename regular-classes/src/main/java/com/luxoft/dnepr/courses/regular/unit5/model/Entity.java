package com.luxoft.dnepr.courses.regular.unit5.model;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 11.11.13
 * Time: 21:31
 * To change this template use File | Settings | File Templates.
 */
public class Entity {
    private Long id;
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Entity(Long id) {
        this.id = id;
    }
}
