package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 06.11.13
 * Time: 22:18
 * To change this template use File | Settings | File Templates.
 */
public class Bank implements BankInterface {
    private Map <Long, UserInterface> users;

    public Map<Long, UserInterface> getUsers() {
          return users;
    }
    public void setUsers(Map<Long, UserInterface> users) {
          this.users = users;
    }

    public Bank(String expectedJavaVersion) throws IllegalJavaVersionError{
        String actualJavaVersion = System.getProperty("java.version");
        if (!actualJavaVersion.equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, "Wrong java version");
        }

        this.users = new HashMap<Long, UserInterface>();
    }

    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        if (!users.containsKey(fromUserId)) {
            throw new NoUserFoundException(fromUserId, "The user with id "+fromUserId+" didn't found");
        }

        if (!users.containsKey(toUserId)) {
            throw new NoUserFoundException(toUserId, "The user with id "+toUserId+" didn't found");
        }

        UserInterface fromUser = users.get(fromUserId);
        UserInterface toUser = users.get(toUserId);

        WalletInterface fromWallet = fromUser.getWallet();
        WalletInterface toWallet = toUser.getWallet();

        if (isBlocked(fromWallet)) {
            throw new TransactionException("User '" + fromUser.getName() + "' wallet is blocked");
        }

        if (isBlocked(toWallet)) {
            throw new TransactionException("User '" + toUser.getName() + "' wallet is blocked");
        }

        if (fromWallet.getAmount().compareTo(amount)<0) {
            throw  new TransactionException("User '" + fromUser.getName() + "' has insufficient funds (" + decorateCurrency(fromWallet.getAmount()) + " < " + decorateCurrency(amount) + ")");
        }

        if (toWallet.getAmount().add(amount).compareTo(toWallet.getMaxAmount()) > 0 ) {
            throw  new TransactionException("User '" + toUser.getName() + "' wallet limit exceeded (" + decorateCurrency(toWallet.getAmount()) + " + " + decorateCurrency(amount) + " > " + decorateCurrency(toWallet.getMaxAmount()) + ")");
        }

        fromWallet.getAmount().subtract(amount);
        toWallet.getAmount().add(amount);
    }


    private boolean isBlocked (WalletInterface wallet){
        return (wallet.getStatus() != WalletStatus.ACTIVE);
    }

    private String decorateCurrency(BigDecimal amount) {
        DecimalFormat df = new DecimalFormat();

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');

        df.setMaximumFractionDigits(2);
        df.setDecimalFormatSymbols(otherSymbols);
        df.setMinimumFractionDigits(2);
        String result = df.format(amount);

        return result;
    }

}
