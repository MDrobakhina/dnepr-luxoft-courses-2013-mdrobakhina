package com.luxoft.dnepr.courses.regular.unit3.errors;

public class IllegalJavaVersionError extends Error {
    String actualJavaVersion;
    String expectedJavaVersion;
    public IllegalJavaVersionError(String actualJavaVersion, String expectedJavaVersion, String message)  {
        super(message);
        this.actualJavaVersion = actualJavaVersion;
        this.expectedJavaVersion = expectedJavaVersion;

    }

    public String getActualJavaVersion() {
        return actualJavaVersion;
    }

    public String getExpectedJavaVersion() {
        return expectedJavaVersion;
    }
}
