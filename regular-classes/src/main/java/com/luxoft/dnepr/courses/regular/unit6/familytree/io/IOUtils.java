package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;

import java.io.*;

public class IOUtils {
	
	private IOUtils() {
	}
	
	public static FamilyTree load(String filename) throws Exception{
        FileInputStream inputStream = new FileInputStream(filename);
        return load(inputStream);
	}
	
	public static FamilyTree load(InputStream is) throws Exception{
        ObjectInputStream ois = new ObjectInputStream(is);
        FamilyTree familyTree = (FamilyTree) ois.readObject();
        is.close();
        return familyTree;
	}
	
	public static void save(String filename, FamilyTree familyTree) throws Exception{
        FileOutputStream outputStream = new FileOutputStream(filename);
        save(outputStream, familyTree);
    }
	
	public static void save(OutputStream os, FamilyTree familyTree) throws Exception{
        ObjectOutputStream outputStream = new ObjectOutputStream(os);
        outputStream.writeObject(familyTree);
        os.close();
	}
}
