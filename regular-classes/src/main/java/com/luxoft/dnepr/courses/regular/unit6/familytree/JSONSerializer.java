package com.luxoft.dnepr.courses.regular.unit6.familytree;

import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 16.11.13
 * Time: 16:15
 * To change this template use File | Settings | File Templates.
 */
public interface JSONSerializer <E> {
    public void saveObjectToStream(OutputStreamWriter outputStream, E object) throws Exception;
    E objectFromScanner(Scanner scanner) throws Exception;
}
