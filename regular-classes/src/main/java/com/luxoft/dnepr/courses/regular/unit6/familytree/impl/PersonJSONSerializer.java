package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.JSONSerializer;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 16.11.13
 * Time: 19:30
 * To change this template use File | Settings | File Templates.
 */
public class PersonJSONSerializer implements JSONSerializer<Person> {
    @Override
    public Person objectFromScanner(Scanner scanner) {

        Person person = new PersonImpl();

        while (scanner.hasNext()) {
            String next = scanner.next();
            String[] keysAndValues = next.split(":");
            if (keysAndValues.length > 1) {
                scanProperty(person, keysAndValues[0].replaceAll("\"", ""), keysAndValues[1].replaceAll("\"", ""), scanner);
            } else {
                String key = keysAndValues[0].replaceAll("\"", "");
                if (key.equals(""))
                    break;
                else
                    scanProperty(person, keysAndValues[0].replaceAll("\"", ""), null, scanner);
            }
        }

        return person;
    }

    @Override
    public void saveObjectToStream(OutputStreamWriter streamWriter, Person object) throws Exception{
        String str = stringFromPerson(object);
        streamWriter.write(str);
    }

    private String stringFromPerson(Person object) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\"name\":\"" + object.getName() +
                "\",\"ethnicity\":\"" + object.getEthnicity() +
                "\",\"age\":\"" + object.getAge() +
                "\",\"gender\":\"" + object.getGender().getName() +
                "\"");

        if (object.getFather() != null) {
            stringBuilder.append(",\"father\":{");
            stringBuilder.append(stringFromPerson(object.getFather()));
            stringBuilder.append("}");
        }

        if (object.getMother() != null) {
            stringBuilder.append(",\"mother\":{");
            stringBuilder.append(stringFromPerson(object.getMother()));
            stringBuilder.append("}");
        }

        return stringBuilder.toString();
    }

    public void scanProperty(Person person, String key, String value, Scanner scanner) {
        if (key.equals("father")) {
            PersonJSONSerializer personJSONSerializer = new PersonJSONSerializer();
            Person father = personJSONSerializer.objectFromScanner(scanner);
            person.setFather(father);
        } else if (key.equals("mother")) {
            PersonJSONSerializer personJSONSerializer = new PersonJSONSerializer();
            Person mother = personJSONSerializer.objectFromScanner(scanner);
            person.setMother(mother);
        } else if (key.equals("name")) {
            person.setName(value);
        } else if (key.equals("ethnicity")) {
            person.setEthnicity(value);
        } else if (key.equals("age")) {
            person.setAge(Integer.parseInt(value));
        } else if (key.equals("gender")) {
            if (value.equals(Gender.MALE.getName()))
                person.setGender(Gender.MALE);
            else if (value.equals(Gender.FEMALE.getName()))
                person.setGender(Gender.FEMALE);
        }
    }
}


