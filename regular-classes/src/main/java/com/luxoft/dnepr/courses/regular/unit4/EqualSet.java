package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 09.11.13
 * Time: 11:38
 * To change this template use File | Settings | File Templates.
 */
public class EqualSet<E> implements Set<E> {

    private ArrayList<E> contents;

    public EqualSet(Collection<? extends E> collection) {
        contents = new ArrayList<E>();

        if (collection != null) {
            for (E e : collection) {
                add(e);
            }
        }
    }

    public EqualSet() {
        contents = new ArrayList<E>();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return contents.toArray(a);
    }

    @Override
    public void clear() {
        contents.clear();
    }

    @Override
    public int size() {
        return contents.size();
    }

    @Override
    public boolean contains(Object o) {
        return contents.contains(o);
    }

    @Override
    public boolean isEmpty() {
        return contents.isEmpty();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return contents.retainAll(c);
    }

    @Override
    public Iterator<E> iterator() {
        return contents.iterator();
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean result = false;

        if (c != null) {
            for (E e : c) {
                if (add(e))
                    result = true;
            }
        }

        return result;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return contents.containsAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return contents.removeAll(c);
    }

    @Override
    public Object[] toArray() {
        return contents.toArray();
    }

    @Override
    public boolean remove(Object o) {
        return contents.remove(o);
    }

    @Override
    public boolean add(E e) {
        if (!contents.contains(e)) {
            return contents.add(e);
        }

        return false;
    }
}
