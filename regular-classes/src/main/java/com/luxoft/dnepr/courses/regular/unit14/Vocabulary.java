package com.luxoft.dnepr.courses.regular.unit14;

import java.util.HashSet;

public class Vocabulary implements TextParserDelegate {
    private HashSet<String> voc = new HashSet<String>();

    public Vocabulary() {

    }

    public void fillFromResource(String resource) {
        TextParser parser = new TextParser(resource, this);
        parser.parse();
    }

    public void add(String word) {
        voc.add(word.toLowerCase());
    }

    public String random() {
        if (voc.size() > 0)
            return voc.toArray(new String[]{})[(int) (Math.random() * voc.size())];
        else return null;
    }

    public int size() {
        return voc.size();
    }

    @Override
    public void parserDidFindWord(String word) {
        add(word);
    }
}
