package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FileFilter;
import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 23.11.13
 * Time: 14:18
 * To change this template use File | Settings | File Templates.
 */
public class FileProducer implements Callable<HashSet<File>> {

    private final BlockingQueue<String> fileQueue;
    private final FileFilter fileFilter;
    private final File root;
    private final HashSet <File> processedFiles = new HashSet <File> ();

    public FileProducer(BlockingQueue<String>fileQueue, FileFilter fileFilter, File root) {
        this.fileQueue = fileQueue;
        this.fileFilter = fileFilter;
        this.root = root;
    }

    private void crawl(File root) throws InterruptedException {
        File[] entries = root.listFiles(fileFilter);
        if (entries != null) {
            for (File entry : entries)
                if (entry.isDirectory())
                    crawl(entry);
                else if (!alreadyIndexed(entry)) {
                    fileQueue.put(entry.getAbsolutePath());
                    processedFiles.add(entry);
                }
        }
    }

    private boolean alreadyIndexed(File file) {
        return processedFiles.contains(file);
    }

    @Override
    public HashSet<File> call() throws Exception {
        try {
            crawl(root);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        fileQueue.put("q");

        return processedFiles;
    }
}
