package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Scanner;

public class UI implements Runnable {
    public void run() {
        String randomWord, answer;

        Vocabulary vocabulary = new Vocabulary();
        vocabulary.fillFromResource("sonnets.txt");
        if (vocabulary.size() == 0) {
            System.out.println("Error. Empty vocabulary.");
            return;
        }
        printInfo();
        for (; ; ) {
            randomWord = vocabulary.random();

            if (randomWord == null) {
                System.out.println("Error. Empty vocabulary.");
                return;
            }

            System.out.println("Do you know translation of this word?:");
            System.out.println(randomWord);

// get a boolean here, null for errors
            Boolean result = null;
            Scanner s = new Scanner(System.in);
            answer = s.next();
            if (answer != null && (answer.isEmpty() || answer.equals("exit"))) {
                Words.printResult(vocabulary.size());
                return;
            }
            if (answer != null && (answer.isEmpty() || answer.equals("help"))) {
                printInfo();
                continue;
            }
            if (answer != null) result = answer.equalsIgnoreCase("Y");
            if (result != null) {
            } else {
                System.out.println("Error while reading answer");
                return;
            }
            Words.markWord(randomWord, result);
        }
    }

    public static void printInfo() {
        System.out.println("Welcome to Linguistic analizator. You can check your language knowledge");
        System.out.println("If you want to read rules again, type \"help\" ");
        System.out.println("If you know the word, type \"y\", otherwise type \"n\", any other answer will be \"no\"");
        System.out.println("If you want to exit, type \"exit\"");
    }

    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }
}
