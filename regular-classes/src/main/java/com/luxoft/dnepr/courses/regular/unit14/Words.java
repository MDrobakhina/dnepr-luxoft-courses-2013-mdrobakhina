package com.luxoft.dnepr.courses.regular.unit14;
import java.util.Vector;

public class Words {
    public static Vector<String> knownWords = new Vector();
    public static Vector<String> unknownWords = new Vector();

    public static void printResult(int tot) {
        int estimatedWords = tot * (knownWords.size() + 1) / (knownWords.size() + unknownWords.size() + 1);
        System.out.println("Your estimated vocabulary is " + estimatedWords + " words");
    }

    public static void markWord (String word, Boolean doKnow) {
        if (doKnow) {
            knownWords.add(word);
        } else {
            unknownWords.add(word);
        }

    }
}
