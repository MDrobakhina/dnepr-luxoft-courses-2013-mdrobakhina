package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.JSONSerializer;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class FamilyTreeImpl implements FamilyTree {

	private static final long serialVersionUID = 3057396458981676327L;
	private Person root;
	private transient long creationTime;
	
	private FamilyTreeImpl(Person root, long creationTime) {
		this.root = root;
		this.creationTime = creationTime;
	}
	
	public static FamilyTree create(Person root) {
		return new FamilyTreeImpl(root, System.currentTimeMillis());
	}
	
	@Override
	public Person getRoot() {
		return root;
	}
	
	@Override
	public long getCreationTime() {
		return creationTime;
	}

    private void writeObject(ObjectOutputStream out) throws IOException {
        JSONSerializer<FamilyTree> jsonSerializer = new FamilyTreeJSONSerializer();
        try {
            jsonSerializer.saveObjectToStream(new OutputStreamWriter(out), this);
        } catch (Exception exc) {
            throw new IOException();
        }
    }

    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException {
        try {
            Scanner scanner = new Scanner(is);
            scanner.useDelimiter("[{},]");
            scanner.next();


            PersonJSONSerializer jsonSerializer = new PersonJSONSerializer();
            Person root = jsonSerializer.objectFromScanner(scanner);
            scanner.close();
            this.root = root;

        } catch (Exception exc) {
            throw new IOException();
        }
    }
}
