package com.luxoft.dnepr.courses.regular.unit6.familytree;

public enum Gender {
	MALE("MALE"),
    FEMALE("FEMALE");

    private String name;

    public String getName() {
        return name;
    }
    private Gender(String name) {
        this.name=name;
    }
}
