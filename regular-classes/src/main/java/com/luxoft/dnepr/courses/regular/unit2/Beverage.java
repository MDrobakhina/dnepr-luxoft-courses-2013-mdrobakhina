package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {


    boolean nonAlcoholic;

    public Beverage() {
        super("", 0, "");
        this.nonAlcoholic = true;
    }

    public Beverage(String code, double price, String name, boolean nonAlcoholic)
    {
        super(code, price, name);
        this.nonAlcoholic = nonAlcoholic;
    }


    public void setNonAlcoholic(boolean nonAlcoholic) {
        this.nonAlcoholic = nonAlcoholic;
    }
    public boolean isNonAlcoholic() {
        return   nonAlcoholic;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (nonAlcoholic != beverage.nonAlcoholic) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nonAlcoholic ? 1 : 0);
        return result;
    }
}
