package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents word statistics storage.
 */
public class WordStorage {

    //private ConcurrentHashMap<String, Integer> wordsStatistics = new ConcurrentHashMap<String, Integer>();
    private ConcurrentHashMap<String, Integer> wordsStatistics = new ConcurrentHashMap<String, Integer>();
    /**
     * Saves given word and increments count of occurrences.
     * @param word
     */
    public synchronized void save(String word) {
        if (wordsStatistics.containsKey(word)) {
            wordsStatistics.put(word, new Integer(wordsStatistics.get(word).intValue() + 1));
        } else {
            wordsStatistics.put(word, new Integer(1));
        }
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(wordsStatistics);
    }
}
