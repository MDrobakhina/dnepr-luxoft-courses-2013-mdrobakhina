package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */

    public Product firstChild() {
        return getAmount() > 0 ? childProducts.get(0) : null;
    }

    @Override
    public String getCode() {
        Product firstChild = firstChild();
        return firstChild != null ? firstChild.getCode() : null;
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */

    @Override
    public String getName() {
        Product firstChild = firstChild();
        return firstChild != null ? firstChild.getName() : null;
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */

    @Override
    public double getPrice() {
        double price = 0;

        for (Product product : childProducts) {
            price += product.getPrice();
        }

        price -= price * getDiscount();

        return price;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    protected double getDiscount() {
        double discount = 0;
        int amount = getAmount();

        if (amount > 2) {
            discount = 0.1;
        } else if (amount == 2) {
            discount = 0.05;
        }

        return discount;
    }


    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
