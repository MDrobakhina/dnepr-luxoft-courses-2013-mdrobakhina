package com.luxoft.dnepr.courses.regular.unit2;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 02.11.13
 * Time: 12:26
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractProduct implements Product, Cloneable {
    String code;
    double price;
    String name;

    protected AbstractProduct(String code, double price, String name) {
        this.code = code;
        this.price = price;
        this.name = name;
    }

    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractProduct that = (AbstractProduct) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;

    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price>0) {
            this.price = price;
        } else {
            this.price = 0;
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
