package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;


public class LuxoftUtilsTest {
    @Test
    public void sortArrayTest() {
        String[] arr1 = new String[]{"cat", "Cat", "absolute", "dog", "rabbit"};
        String[] arr2 = new String[]{"Cat", "absolute", "cat", "dog", "rabbit"};
        String[] arr3 = new String[]{"rabbit", "dog", "cat", "absolute", "Cat"};
        String[] arr4 = new String[]{"ab   ", "D", "S", "asure"};
        String[] arr5 = new String[]{"D", "S", "ab   ", "asure"};
        Assert.assertArrayEquals(arr2, LuxoftUtils.sortArray(arr1, true));
        Assert.assertArrayEquals(arr3, LuxoftUtils.sortArray(arr1, false));
        Assert.assertArrayEquals(arr5, LuxoftUtils.sortArray(arr4, true));

    }

    @Test
    public void wordAverageLengthTest() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat"), 0);
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I    have a   cat"), 0);
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat "), 0);
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("  I have a cat "), 0);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(""), 0);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(" "), 0);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength("   "), 0);
        Assert.assertEquals(4, LuxoftUtils.wordAverageLength("   test "), 0);


    }

    @Test
    public void calculateOverallAreaTest() {
        ArrayList<Figure> list = new ArrayList<Figure>();
        list.add(new Hexagon(0));
        list.add(new Square(6));
        list.add(new Circle(1));
        list.add(new Square(2));
        Assert.assertEquals(43.1415926535898, LuxoftUtils.calculateOverallArea(list), 0.000001);
        Assert.assertEquals(43.1415926, LuxoftUtils.calculateOverallArea(list), 0.000001);
    }

    @Test
    public void reverseWordsTest() {
        Assert.assertTrue(LuxoftUtils.reverseWords("abc def").equals("cba fed"));
        Assert.assertTrue(LuxoftUtils.reverseWords("abc m    def").equals("cba m    fed"));
        Assert.assertTrue(LuxoftUtils.reverseWords("abc m    def ").equals("cba m    fed "));
        Assert.assertTrue(LuxoftUtils.reverseWords("").equals(""));
        Assert.assertTrue(LuxoftUtils.reverseWords(" ").equals(" "));
        Assert.assertTrue(LuxoftUtils.reverseWords(" abc def").equals(" cba fed"));
        Assert.assertTrue(LuxoftUtils.reverseWords("ab def").equals("ba fed"));

    }


}
