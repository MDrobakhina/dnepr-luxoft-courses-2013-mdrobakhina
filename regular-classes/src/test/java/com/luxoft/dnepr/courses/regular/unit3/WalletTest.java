package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 07.11.13
 * Time: 0:34
 * To change this template use File | Settings | File Templates.
 */
public class WalletTest {
    @Test
    public void testCheckWithdrawal() throws Exception {
        try {

            Wallet fromWallet = new Wallet(1, 100, 1000, WalletStatus.ACTIVE);
            Wallet toWallet = new Wallet(2, 200, 199, WalletStatus.ACTIVE);
               fromWallet.checkWithdrawal(new BigDecimal(200));
        } catch (InsufficientWalletAmountException e) {
            Assert.assertEquals(e.getMessage(), "Your balance is exceeded");
        }
    }
    public void testCheckTransfer() throws Exception {
        try {
            Wallet fromWallet = new Wallet(1, 100, 1000, WalletStatus.ACTIVE);
            Wallet toWallet = new Wallet(2, 200, 199, WalletStatus.ACTIVE);
            toWallet.checkTransfer(new BigDecimal(200));
        } catch (LimitExceededException e) {
            Assert.assertEquals(e.getMessage(), "Your limit is exceeded");
        }
    }
}
