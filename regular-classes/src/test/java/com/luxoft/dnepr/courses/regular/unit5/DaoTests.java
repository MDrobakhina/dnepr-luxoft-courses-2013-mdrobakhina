package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 11.11.13
 * Time: 23:28
 * To change this template use File | Settings | File Templates.
 */
public class DaoTests {
    @Test
    public void testDaoSave() throws Exception {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        IDao<Redis> redisIDao = new RedisDaoImpl();
        Redis redis = new Redis(new Long(1), 10);
        Employee employee = new Employee(new Long(2), 200);
        employeeIDao.save(employee);
        redisIDao.save(redis);

        Assert.assertTrue(EntityStorage.getEntities().size() == 2);
    }

    @Test
    public void testDaoSaveNull() throws Exception {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        IDao<Redis> redisIDao = new RedisDaoImpl();
        Redis redis = new Redis(null, 10);
        Employee employee = new Employee(new Long(2), 200);
        employeeIDao.save(employee);
        redisIDao.save(redis);

        Assert.assertTrue(redis.getId().equals(new Long(3)));
        Assert.assertTrue(EntityStorage.getEntities().size() == 2);

    }
    @Test
    public void testDaoUpdate() throws Exception {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        IDao<Redis> redisIDao = new RedisDaoImpl();
        Redis redis1 = new Redis(new Long (1), 10);
        Redis redis2 = new Redis(new Long (1), 20);
        Employee employee = new Employee(new Long(2), 200);
        employeeIDao.save(employee);
        redisIDao.save(redis1);
        redisIDao.update(redis2);
        Assert.assertTrue(redisIDao.get(new Long(1)).getWeight() == 20);
    }
    @Test
    public void testDaoUpdateException() throws Exception {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        IDao<Redis> redisIDao = new RedisDaoImpl();
        Redis redis1 = new Redis(new Long (1), 10);
        Redis redis3 = new Redis(new Long (5), 20);
        Employee employee = new Employee(new Long(2), 200);
        employeeIDao.save(employee);
        redisIDao.save(redis1);
        try {
            redisIDao.update(redis3);
        } catch (UserNotFound  e) {
            Assert.assertEquals(e.getMessage(), "User not found");
        }
    }
    @Test
    public void testDaoDelete() throws Exception {
        IDao<Employee> employeeIDao = new EmployeeDaoImpl();
        IDao<Redis> redisIDao = new RedisDaoImpl();
        Redis redis1 = new Redis(new Long (1), 10);
        Employee employee = new Employee(new Long(2), 200);
        employeeIDao.save(employee);
        redisIDao.save(redis1);
        Assert.assertTrue(redisIDao.delete(new Long(1)));
        Assert.assertTrue(!redisIDao.delete(new Long(5)));
    }
}
