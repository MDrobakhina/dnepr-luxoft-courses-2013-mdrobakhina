package com.luxoft.dnepr.courses.regular.unit6;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 16.11.13
 * Time: 20:48
 * To change this template use File | Settings | File Templates.
 */
public class IOUtilsTests {
    @Test
    public void testSave() throws Exception {
        Person father = new PersonImpl(null, "wee", null, null, Gender.MALE, 22);
        Person mother = new PersonImpl("xcv", "sdf", new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), null, Gender.FEMALE, 22);
        Person person = new PersonImpl("asd", "dsa", father, mother, Gender.MALE, 21);
        FamilyTree familyTree = FamilyTreeImpl.create(person);
        IOUtils.save("file.txt", familyTree);
    }

    @Test
    public void testRead() throws Exception {

        Person father = new PersonImpl(null, "wee", new PersonImpl("xcv", "sdf", new PersonImpl("wer", "ewwe", new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), new PersonImpl("wer", "ewwe", new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), Gender.MALE, 22), Gender.MALE, 22), null, Gender.FEMALE, 22), new PersonImpl("xcv", "sdf", new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), new PersonImpl("wer", "ewwe", new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), Gender.MALE, 22), Gender.FEMALE, 22), Gender.MALE, 22);
        Person mother = new PersonImpl("xcv", "sdf", new PersonImpl("wer", "ewwe", new PersonImpl("xcv", "sdf", new PersonImpl("wer", "ewwe", new PersonImpl("xcv", "sdf", new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), null, Gender.FEMALE, 22), null, Gender.MALE, 22), new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), Gender.FEMALE, 22), null, Gender.MALE, 22), new PersonImpl("xcv", "sdf", new PersonImpl("wer", "ewwe", new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), new PersonImpl("wer", "ewwe", null, null, Gender.MALE, 22), Gender.MALE, 22), null, Gender.FEMALE, 22), Gender.FEMALE, 22);
        Person person = new PersonImpl(null, "dsa", father, mother, Gender.MALE, 21);

        FamilyTree familyTree = FamilyTreeImpl.create(person);
        IOUtils.save("file.txt", familyTree);

        FamilyTree familyTree2 = IOUtils.load("file.txt");

        String s1 = familyTree.getRoot().toString();
        String s2 = familyTree2.getRoot().toString();

        Assert.assertTrue(familyTree.getRoot().toString().equals(familyTree2.getRoot().toString()));
    }
}
