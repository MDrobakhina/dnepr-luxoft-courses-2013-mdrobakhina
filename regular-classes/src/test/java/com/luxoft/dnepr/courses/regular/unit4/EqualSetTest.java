package com.luxoft.dnepr.courses.regular.unit4;

import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 09.11.13
 * Time: 13:33
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {
    @Test
    public void testCreateSet () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();
        Assert.assertTrue(integersSet.size() == 0);
        Assert.assertTrue(integersSet.isEmpty());
    }

    @Test
    public void testCreateSetBasedOnCollection () throws Exception {
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.add(new Integer(1));
        array.add(new Integer(2));
        array.add(new Integer(5));
        array.add(new Integer(3));
        array.add(new Integer(3));
        array.add(null);
        array.add(null);

        EqualSet <Integer> integersSet = new EqualSet <Integer>(array);
        Assert.assertTrue(integersSet.size() == 5);
    }

    @Test
    public void testAdd () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();
        integersSet.add(null);
        integersSet.add(new Integer(1));
        integersSet.add(new Integer(2));
        integersSet.add(null);
        integersSet.add(new Integer(4));
        integersSet.add(new Integer(4));
        integersSet.add(null);
        integersSet.add(new Integer(4));

        Assert.assertTrue(integersSet.size()== 4);
        Assert.assertTrue(integersSet.contains(null));
        Assert.assertTrue(integersSet.contains(new Integer(1)));
        Assert.assertTrue(integersSet.contains(new Integer(2)));
        Assert.assertTrue(integersSet.contains(new Integer(4)));
    }

    public void testRemove () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();
        integersSet.add(null);
        integersSet.add(new Integer(1));
        integersSet.add(new Integer(2));
        integersSet.add(null);
        integersSet.add(new Integer(4));
        integersSet.add(new Integer(4));
        integersSet.add(null);
        integersSet.add(new Integer(4));

        Assert.assertTrue(integersSet.size()== 4);
        Assert.assertTrue(integersSet.contains(null));
        Assert.assertTrue(integersSet.contains(new Integer(1)));
        Assert.assertTrue(integersSet.contains(new Integer(2)));
        Assert.assertTrue(integersSet.contains(new Integer(4)));

        integersSet.remove(null);

        Assert.assertTrue(integersSet.size() == 3);
        Assert.assertFalse(integersSet.contains(null));

        integersSet.clear();
        Assert.assertTrue(integersSet.size() == 0);
    }

    @Test
    public void testAddAll () throws Exception {
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.add(new Integer(1));
        array.add(new Integer(2));
        array.add(new Integer(5));
        array.add(new Integer(3));
        array.add(new Integer(3));
        array.add(null);
        array.add(null);

        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.addAll(array);

        Assert.assertTrue(integersSet.size() == 5);
    }

    @Test
    public void testToArray () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.add(null);
        integersSet.add(new Integer(1));
        integersSet.add(new Integer(3));
        integersSet.add(new Integer(5));

        Integer[] integersArray;

        integersArray = integersSet.toArray(new Integer[0]);

        Assert.assertTrue(integersArray.length == 4);
    }

    @Test
    public void testIntersect () throws Exception {
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.add(new Integer(1));
        array.add(new Integer(2));
        array.add(new Integer(5));
        array.add(new Integer(3));
        array.add(new Integer(3));
        array.add(new Integer(6));
        array.add(null);
        array.add(null);


        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.add(null);
        integersSet.add(new Integer(1));
        integersSet.add(new Integer(3));
        integersSet.add(new Integer(5));
        integersSet.add(new Integer(7));

        Assert.assertTrue(integersSet.retainAll(array));
        Assert.assertTrue(integersSet.size() == 4);
        Assert.assertTrue(integersSet.contains(null));
        Assert.assertTrue(integersSet.contains(new Integer(1)));
        Assert.assertTrue(integersSet.contains(new Integer(3)));
        Assert.assertTrue(integersSet.contains(new Integer(5)));
    }

    @Test
    public void testInitWithNull () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>(null);
        Assert.assertTrue(integersSet.size() == 0);
    }

    @Test
    public void testAddAllFromNull () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.addAll(null);

        Assert.assertTrue(integersSet.size() == 0);
    }

    @Test
    public void testRetainAllFromNull () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.retainAll(null);

        Assert.assertTrue(integersSet.size() == 0);
    }

    @Test
    public void testRemoveAllFromNull () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.removeAll(null);

        Assert.assertTrue(integersSet.size() == 0);
    }

    @Test
    public void testIterator () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.add(null);
        integersSet.add(new Integer(1));
        integersSet.add(new Integer(3));
        integersSet.add(new Integer(5));
        integersSet.add(new Integer(7));

        Iterator <Integer> iterator = integersSet.iterator();

        Assert.assertTrue(iterator.next() == null);
        Assert.assertTrue(iterator.next().equals(new Integer(1)));
        Assert.assertTrue(iterator.next().equals(new Integer(3)));
        Assert.assertTrue(iterator.next().equals(new Integer(5)));
        Assert.assertTrue(iterator.next().equals(new Integer(7)));
    }

    @Test
    public void testRemoveAll () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.add(null);
        integersSet.add(new Integer(1));
        integersSet.add(new Integer(3));
        integersSet.add(new Integer(5));
        integersSet.add(new Integer(7));

        ArrayList<Integer> array = new ArrayList<Integer>();
        array.add(new Integer(1));
        array.add(new Integer(2));
        array.add(new Integer(5));
        array.add(new Integer(3));
        array.add(new Integer(3));
        array.add(new Integer(6));
        array.add(null);
        array.add(null);

        integersSet.removeAll(array);

        Assert.assertTrue(integersSet.size() == 1);
    }

    public void testRemoveTwoNulls () throws Exception {
        EqualSet <Integer> integersSet = new EqualSet <Integer>();

        integersSet.add(null);
        integersSet.add(new Integer(1));
        integersSet.add(new Integer(3));
        integersSet.add(new Integer(5));
        integersSet.add(new Integer(7));

        integersSet.remove(null);
        integersSet.remove(null);

        Assert.assertFalse(integersSet.contains(null));
    }

}
