package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {

    @Test
    public void getMonthNameTest() {
        Assert.assertEquals("January", LuxoftUtils.getMonthName(1,"en"));
        Assert.assertEquals("Январь", LuxoftUtils.getMonthName(1,"ru"));
        Assert.assertEquals("Unknown language", LuxoftUtils.getMonthName(1,"fgfh5"));
        Assert.assertEquals("Unknown month", LuxoftUtils.getMonthName(-9,"en"));
        Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName(-9,"ru"));
        Assert.assertEquals("Unknown language", LuxoftUtils.getMonthName(-9,"rлол"));

    }

    @Test
    public void binaryToDecimalTest() {
        Assert.assertEquals("12", LuxoftUtils.binaryToDecimal("1100"));
        Assert.assertEquals("0", LuxoftUtils.binaryToDecimal("0000"));
        Assert.assertEquals("3", LuxoftUtils.binaryToDecimal("011"));
        Assert.assertEquals("16", LuxoftUtils.binaryToDecimal("10000"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("6797"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("o10101"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("-011"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(""));
    }
    @Test
    public void decimalToBinaryTest() {
        Assert.assertEquals("1010", LuxoftUtils.decimalToBinary("10"));
        Assert.assertEquals("11", LuxoftUtils.decimalToBinary("3"));
        Assert.assertEquals("0", LuxoftUtils.decimalToBinary("0000"));
        Assert.assertEquals("0", LuxoftUtils.decimalToBinary("0"));

        Assert.assertEquals("100000", LuxoftUtils.decimalToBinary("32"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("abcd"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("o1"));
        Assert.assertEquals("101", LuxoftUtils.decimalToBinary("-5"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(""));
    }
    @Test
    public void sortArrayTest() {
        int[] arr1=new int[] {3,2,1};
        int[] arr2=new int[] {1,2,3};

        Assert.assertArrayEquals(arr2, LuxoftUtils.sortArray(arr1,true));
        Assert.assertArrayEquals(arr1, LuxoftUtils.sortArray(arr2,false));

        int[] arr3=new int[] {2,2,2};
        int[] arr4=new int[] {2,2,2};
        Assert.assertArrayEquals(arr3, LuxoftUtils.sortArray(arr4,false));


    }



}


