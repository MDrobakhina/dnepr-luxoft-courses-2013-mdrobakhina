package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: BVS
 * Date: 06.11.13
 * Time: 23:03
 * To change this template use File | Settings | File Templates.
 */
public class BankTest {
    @Test
    public void testBank() throws Exception {
        try {
            Bank bank = new Bank("423423");
        } catch (IllegalJavaVersionError e) {
            Assert.assertEquals(e.getMessage(), "Wrong java version");
        }
    }

    @Test
    public void testMakeMoneyTransactionLimitExceeded() throws Exception {
        try {
            Bank bank = new Bank("1.7.0_40");
            Wallet fromWallet = new Wallet(1, 100, 1000, WalletStatus.ACTIVE);
            Wallet toWallet = new Wallet(2, 100, 199, WalletStatus.ACTIVE);
            User fromUser = new User(1, "From User", fromWallet);
            User toUser = new User(2, "To User", toWallet);
            bank.getUsers().put(fromUser.getId(), fromUser);
            bank.getUsers().put(toUser.getId(), toUser);
            bank.makeMoneyTransaction(new Long(1), new Long(2), new BigDecimal(100));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), "User 'To User' wallet limit exceeded (100.00 + 100.00 > 199.00)");
        }
    }

    @Test
    public void testMakeMoneyTransactionUserExists() throws Exception {
        try {
            Bank bank = new Bank("1.7.0_40");
            Wallet fromWallet = new Wallet(1, 100, 1000, WalletStatus.ACTIVE);
            Wallet toWallet = new Wallet(2, 100, 199, WalletStatus.ACTIVE);
            User fromUser = new User(1, "From User", fromWallet);
            User toUser = new User(2, "To User", toWallet);
            //bank.getUsers().put(fromUser.getId(), fromUser);
            bank.getUsers().put(toUser.getId(), toUser);
            bank.makeMoneyTransaction(new Long(1), new Long(2), new BigDecimal(100));
        } catch (NoUserFoundException e) {
            Assert.assertEquals(e.getMessage(), "The user with id 1 didn't found");
        }
    }
}
