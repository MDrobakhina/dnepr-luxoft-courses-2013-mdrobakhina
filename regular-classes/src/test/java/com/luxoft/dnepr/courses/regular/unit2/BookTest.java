package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();
           cloned.setPublicationDate(new GregorianCalendar(2007, 0, 1).getTime());
        assertEquals(new GregorianCalendar(2007, 0, 1).getTime(),cloned.getPublicationDate());
    }

    @Test
    public void testEquals() throws Exception {
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 205, new GregorianCalendar(2006, 0, 1).getTime());
        Book book3 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2007, 0, 1).getTime());
        assertTrue(book1.equals(book2));
        assertTrue(!book1.equals(book3));

    }
}
